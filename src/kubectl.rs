//
// Copyright (c) 2022 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use std::collections::{BTreeMap, BTreeSet};
use std::fmt;

use itertools::Itertools;
use k8s_openapi::apimachinery;
use k8s_openapi::ByteString;
use kube::api;
use kube::ResourceExt;
use serde::{de, ser};

use super::*;

#[cfg(feature = "capi")]
mod capi;
mod helper;
#[cfg(feature = "runtime")]
mod runtime;

#[derive(Clone)]
pub struct Kubectl {
    client: kube::Client,
    manager: String,
}

impl Kubectl {
    #[must_use]
    pub fn from_client(client: kube::Client) -> Self {
        Self {
            client,
            manager: "default-manager".to_string(),
        }
    }

    pub async fn try_default() -> kube::Result<Self> {
        kube::Client::try_default().await.map(Self::from_client)
    }

    pub fn from_config(config: kube::Config) -> kube::Result<Self> {
        kube::Client::try_from(config).map(Self::from_client)
    }

    #[must_use]
    pub fn manager(self, manager: &str) -> Self {
        Self {
            manager: manager.to_string(),
            ..self
        }
    }

    pub fn get_manager(&self) -> &str {
        &self.manager
    }

    pub fn client(&self) -> kube::Client {
        self.client.clone()
    }

    pub async fn apiserver_version(&self) -> kube::Result<apimachinery::pkg::version::Info> {
        self.client.apiserver_version().await
    }

    pub async fn load_text_from_secret(&self, name: &str, value: &str) -> kube::Result<String> {
        let text = self
            .get_secret(name)
            .await?
            .data
            .unwrap_or_default()
            .get(value)
            .map(|text| String::from_utf8_lossy(&text.0))
            .unwrap_or_default()
            .to_string();
        Ok(text)
    }

    pub async fn ensure_global_k_is_installed<K>(&self, object: K) -> kube::Result<K>
    where
        K: kube::ResourceExt + ser::Serialize + de::DeserializeOwned + Clone + fmt::Debug,
        <K as kube::Resource>::DynamicType: Default,
    {
        self.ensure_global_k_is_installed_impl(object, false).await
    }

    pub async fn ensure_namespaced_k_is_installed<K>(
        &self,
        object: K,
        namespace: &str,
    ) -> kube::Result<K>
    where
        K: kube::Resource + ser::Serialize + de::DeserializeOwned + Clone + fmt::Debug,
        <K as kube::Resource>::DynamicType: Default,
    {
        self.ensure_namespaced_k_is_installed_impl(object, namespace, false)
            .await
    }

    pub async fn ensure_default_namespaced_k_is_installed<K>(&self, object: K) -> kube::Result<K>
    where
        K: kube::Resource + ser::Serialize + de::DeserializeOwned + Clone + fmt::Debug,
        <K as kube::Resource>::DynamicType: Default,
    {
        self.ensure_default_namespaced_k_is_installed_impl(object, false)
            .await
    }

    pub async fn ensure_global_k_is_installed_forced<K>(&self, object: K) -> kube::Result<K>
    where
        K: kube::ResourceExt + ser::Serialize + de::DeserializeOwned + Clone + fmt::Debug,
        <K as kube::Resource>::DynamicType: Default,
    {
        self.ensure_global_k_is_installed_impl(object, true).await
    }

    pub async fn ensure_namespaced_k_is_installed_forced<K>(
        &self,
        object: K,
        namespace: &str,
    ) -> kube::Result<K>
    where
        K: kube::Resource + ser::Serialize + de::DeserializeOwned + Clone + fmt::Debug,
        <K as kube::Resource>::DynamicType: Default,
    {
        self.ensure_namespaced_k_is_installed_impl(object, namespace, true)
            .await
    }

    pub async fn ensure_default_namespaced_k_is_installed_forced<K>(
        &self,
        object: K,
    ) -> kube::Result<K>
    where
        K: kube::Resource + ser::Serialize + de::DeserializeOwned + Clone + fmt::Debug,
        <K as kube::Resource>::DynamicType: Default,
    {
        self.ensure_default_namespaced_k_is_installed_impl(object, true)
            .await
    }

    pub async fn get_k_logs<K>(&self, object: &K) -> kube::Result<String>
    where
        K: kube::Resource + api::Log + de::DeserializeOwned,
        <K as kube::Resource>::DynamicType: Default,
    {
        let name = object.name();
        let lp = self.log_params();
        let objects = if let Some(namespace) = object.namespace() {
            self.namespaced_api::<K>(&namespace)
        } else {
            self.api::<K>()
        };

        objects.logs(&name, &lp).await
    }

    pub async fn list_pods_from_deployment<'a>(
        &self,
        deployment: &str,
        namespace: impl Into<Option<&'a str>>,
    ) -> kube::Result<Vec<corev1::Pod>> {
        let namespace = namespace.into();
        let deployment = self.get_deployment(deployment, namespace).await?;
        let label_selector = deployment
            .spec
            .unwrap_or_default()
            .selector
            .match_labels
            .unwrap_or_default()
            .into_iter()
            // Drop this `key = key` artifact after 1.58 becomes stable
            .map(|(key, value)| format!("{key}={value}", key = key, value = value))
            .join(",");

        let lp = self.list_params().labels(&label_selector);
        self.pods(namespace).list(&lp).await.map(|pods| pods.items)
    }
}

impl fmt::Debug for Kubectl {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Kubectl")
            .field("client", &"<kube::Client>")
            .finish()
    }
}

/// all testing is under ignore since we need a cluster to run against
#[cfg(test)]
mod tests {
    use super::*;
    #[tokio::test]
    #[ignore]
    async fn basic_configmap() {
        let client = Kubectl::try_default().await.unwrap();
        let name = &format!("test-{}", statehub_id::StatehubId::new());
        let cm = client.configmaps(None);

        cm.get(name).await.expect_err("test exists");
        let obj = corev1::ConfigMap::new(name).data([("key", "value")]);
        let data = client.patch(obj, cm.clone()).await;
        let data = data.expect("patch test").data.unwrap();
        let value = data.get("key").unwrap();

        println!("{}", value);
        assert_eq!(value, "value");

        cm.delete(name, &client.delete_params()).await.unwrap();
    }
}
