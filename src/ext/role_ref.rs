//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use super::*;

pub trait RoleRefExt: Sized {
    fn new<T: kube::Resource<DynamicType = ()>>(k: &T) -> Self;
}

impl RoleRefExt for rbacv1::RoleRef {
    fn new<T: kube::Resource<DynamicType = ()>>(k: &T) -> Self {
        let name = k.name();
        let api_group = T::group(&()).to_string();
        let kind = T::kind(&()).to_string();
        Self {
            name,
            api_group,
            kind,
        }
    }
}
