//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use super::*;

pub trait RoleExt: super::ResourceExt {
    fn new(name: impl ToString) -> Self;
    #[must_use]
    fn rules(self, rules: impl IntoIterator<Item = rbacv1::PolicyRule>) -> Self;
}

impl RoleExt for rbacv1::Role {
    fn new(name: impl ToString) -> Self {
        let metadata = Self::metadata(name);
        Self {
            metadata,
            ..Self::default() // rules: todo!(),
        }
    }

    fn rules(self, rules: impl IntoIterator<Item = rbacv1::PolicyRule>) -> Self {
        let rules = Some(rules.into_iter().collect());
        Self { rules, ..self }
    }
}
