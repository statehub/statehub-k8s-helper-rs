//
// Copyright (c) 2022 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use super::*;

pub trait RoleBindingExt: super::ResourceExt {
    fn new<T: IsRole>(name: impl ToString, role: &T) -> Self;
    #[must_use]
    fn subjects(self, subjects: impl IntoIterator<Item = rbacv1::Subject>) -> Self;
}

impl RoleBindingExt for rbacv1::RoleBinding {
    fn new<T: IsRole>(name: impl ToString, role: &T) -> Self
    where
        T: IsRole,
    {
        let metadata = Self::metadata(name);
        let role_ref = rbacv1::RoleRef {
            api_group: <T as kube::Resource>::group(&()).to_string(),
            kind: <T as kube::Resource>::kind(&()).to_string(),
            name: role.name(),
        };
        Self {
            metadata,
            role_ref,
            // subjects: todo!(),
            ..Self::default()
        }
    }

    fn subjects(self, subjects: impl IntoIterator<Item = rbacv1::Subject>) -> Self {
        let subjects = Some(subjects.into_iter().collect());
        Self { subjects, ..self }
    }
}

pub trait IsRole: kube::Resource<DynamicType = ()> {}

impl IsRole for rbacv1::Role {}
impl IsRole for rbacv1::ClusterRole {}
