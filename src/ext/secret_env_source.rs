//
// Copyright (c) 2022 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use super::*;

/// Builders for `corev1::SecretEnvSource` objects
pub trait SecretEnvSourceExt: Sized {
    /// Construct `corev1::SecretEnvSource` object from secret of this name
    fn secret_name(name: impl ToString) -> Self;

    /// Specify whether the Secret must be defined
    #[must_use]
    fn optional(self, yes: bool) -> Self;

    /// Mark this Secret as required (equials to calling .optional(false))
    #[must_use]
    fn required(self) -> Self {
        self.optional(false)
    }
}

impl SecretEnvSourceExt for corev1::SecretEnvSource {
    fn secret_name(name: impl ToString) -> Self {
        let name = Some(name.to_string());
        Self {
            name,
            ..Self::default()
        }
    }

    fn optional(self, yes: bool) -> Self {
        Self {
            optional: Some(yes),
            ..self
        }
    }
}
