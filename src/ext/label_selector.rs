//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use super::*;

pub trait LabelSelectorExt {
    fn all_objects() -> Self;
    fn no_objects() -> Self;
    fn new() -> Self;
    #[must_use]
    fn match_labels(
        self,
        match_labels: impl IntoIterator<Item = (impl ToString, impl ToString)>,
    ) -> Self;
}

impl LabelSelectorExt for metav1::LabelSelector {
    fn new() -> Self {
        Self::default()
    }

    fn all_objects() -> Self {
        Self {
            match_expressions: Some(vec![]),
            match_labels: Some(BTreeMap::new()),
        }
    }

    fn no_objects() -> Self {
        Self {
            match_expressions: None,
            match_labels: None,
        }
    }

    fn match_labels(
        self,
        match_labels: impl IntoIterator<Item = (impl ToString, impl ToString)>,
    ) -> Self {
        let match_labels = match_labels
            .into_iter()
            .map(|(key, value)| (key.to_string(), value.to_string()))
            .collect();
        Self {
            match_labels: Some(match_labels),
            // match_expressions: todo!(),
            ..Self::default()
        }
    }
}
