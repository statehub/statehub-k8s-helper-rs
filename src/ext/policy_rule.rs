//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use super::*;

pub trait PolicyRuleExt: Sized {
    fn new<T: kube::Resource<DynamicType = ()>>() -> Self;
    #[must_use]
    fn api_group(self, group: impl ToString) -> Self;
    #[must_use]
    fn api_groups(self, groups: impl IntoIterator<Item = impl ToString>) -> Self;
    #[must_use]
    fn resource(self, resource: impl ToString) -> Self;
    #[must_use]
    fn resources(self, resources: impl IntoIterator<Item = impl ToString>) -> Self;
    #[must_use]
    fn with_status(self) -> Self;
    #[must_use]
    fn verb(self, verb: impl ToString) -> Self;
    #[must_use]
    fn verbs(self, verbs: impl IntoIterator<Item = impl ToString>) -> Self;
    #[must_use]
    fn all_resources(self) -> Self {
        self.resource("*")
    }
    #[must_use]
    fn all_verbs(self) -> Self {
        self.verb("*")
    }
}

impl PolicyRuleExt for rbacv1::PolicyRule {
    fn new<T: kube::Resource<DynamicType = ()>>() -> Self {
        Self::default()
            .api_group(T::group(&()))
            .resource(T::plural(&()))
    }

    fn api_group(self, group: impl ToString) -> Self {
        let api_groups = Some(vec![group.to_string()]);
        Self { api_groups, ..self }
    }

    fn api_groups(self, groups: impl IntoIterator<Item = impl ToString>) -> Self {
        let api_groups = Some(groups.into_iter().map(|group| group.to_string()).collect());
        Self { api_groups, ..self }
    }

    fn resource(self, resource: impl ToString) -> Self {
        let resources = Some(vec![resource.to_string()]);
        Self { resources, ..self }
    }

    fn resources(self, resources: impl IntoIterator<Item = impl ToString>) -> Self {
        let resources = Some(
            resources
                .into_iter()
                .map(|resource| resource.to_string())
                .collect(),
        );
        Self { resources, ..self }
    }

    fn with_status(mut self) -> Self {
        let add_status = |resource: String| {
            if resource.ends_with("/status") {
                vec![resource]
            } else {
                vec![format!("{}/status", resource), resource]
            }
        };
        let resources = self
            .resources
            .unwrap_or_default()
            .into_iter()
            .flat_map(add_status)
            .collect::<Vec<_>>();
        self.resources = if resources.is_empty() {
            None
        } else {
            Some(resources)
        };
        self
    }

    fn verb(self, verb: impl ToString) -> Self {
        let verbs = vec![verb.to_string()];
        Self { verbs, ..self }
    }

    fn verbs(self, verbs: impl IntoIterator<Item = impl ToString>) -> Self {
        let verbs = verbs.into_iter().map(|verb| verb.to_string()).collect();
        Self { verbs, ..self }
    }
}
