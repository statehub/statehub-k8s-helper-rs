//
// Copyright (c) 2022 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use super::*;

pub trait JobExt: super::ResourceExt + Sized {
    fn new(name: impl ToString) -> Self;
    fn with_labels(
        name: impl ToString,
        labels: impl IntoIterator<Item = (impl ToString, impl ToString)>,
    ) -> Self;

    #[must_use]
    fn active_deadline_seconds(self, seconds: i64) -> Self;
    #[must_use]
    fn backoff_limit(self, limit: i32) -> Self;
    #[must_use]
    fn completion_mode<'a>(self, mode: impl Into<Option<&'a str>>) -> Self;
    #[must_use]
    fn non_indexed(self) -> Self {
        self.completion_mode("NonIndexed")
    }
    #[must_use]
    fn indexed(self) -> Self {
        self.completion_mode("Indexed")
    }
    #[must_use]
    fn completions(self, completions: i32) -> Self;
    #[must_use]
    fn manual_selector(self, yes: bool) -> Self;
    fn parallelism(self, parallelism: i32) -> Self;

    #[must_use]
    fn selector(self, selector: metav1::LabelSelector) -> Self;
    #[must_use]
    fn match_labels(
        self,
        match_labels: impl IntoIterator<Item = (impl ToString, impl ToString)>,
    ) -> Self;
    #[must_use]
    fn suspend(self, yes: bool) -> Self;
    #[must_use]
    fn template(self, template: corev1::PodTemplateSpec) -> Self;

    #[must_use]
    fn pod(self, pod: corev1::PodSpec) -> Self;
    #[must_use]
    fn ttl_seconds_after_finished(self, seconds: i32) -> Self;
}

impl JobExt for batchv1::Job {
    fn new(name: impl ToString) -> Self {
        let metadata = Self::metadata(name);
        Self {
            metadata,
            // spec: todo!(),
            // status: todo!(),
            ..Self::default()
        }
    }

    fn with_labels(
        name: impl ToString,
        labels: impl IntoIterator<Item = (impl ToString, impl ToString)>,
    ) -> Self {
        let mut job = Self::new(name);
        let labels = labels
            .into_iter()
            .map(|(key, value)| (key.to_string(), value.to_string()));
        job.labels_mut().extend(labels);
        job
    }

    fn active_deadline_seconds(self, seconds: i64) -> Self {
        let mut spec = self.spec.unwrap_or_default();
        spec.active_deadline_seconds = Some(seconds);
        Self {
            spec: Some(spec),
            ..self
        }
    }

    fn backoff_limit(self, limit: i32) -> Self {
        let mut spec = self.spec.unwrap_or_default();
        spec.backoff_limit = Some(limit);
        Self {
            spec: Some(spec),
            ..self
        }
    }

    fn completion_mode<'a>(self, mode: impl Into<Option<&'a str>>) -> Self {
        let mut spec = self.spec.unwrap_or_default();
        spec.completion_mode = mode.into().map(|mode| mode.to_string());
        Self {
            spec: Some(spec),
            ..self
        }
    }

    fn completions(self, completions: i32) -> Self {
        let mut spec = self.spec.unwrap_or_default();
        spec.completions = Some(completions);
        Self {
            spec: Some(spec),
            ..self
        }
    }

    fn manual_selector(self, yes: bool) -> Self {
        let mut spec = self.spec.unwrap_or_default();
        spec.manual_selector = Some(yes);
        Self {
            spec: Some(spec),
            ..self
        }
    }

    fn parallelism(self, parallelism: i32) -> Self {
        let mut spec = self.spec.unwrap_or_default();
        spec.parallelism = Some(parallelism);
        Self {
            spec: Some(spec),
            ..self
        }
    }

    fn selector(self, selector: metav1::LabelSelector) -> Self {
        let mut spec = self.spec.unwrap_or_default();
        spec.selector = Some(selector);
        Self {
            spec: Some(spec),
            ..self
        }
    }

    fn match_labels(
        self,
        match_labels: impl IntoIterator<Item = (impl ToString, impl ToString)>,
    ) -> Self {
        let mut spec = self.spec.unwrap_or_default();
        let selector = spec.selector.unwrap_or_default().match_labels(match_labels);
        spec.selector = Some(selector);
        Self {
            spec: Some(spec),
            ..self
        }
    }

    fn suspend(self, yes: bool) -> Self {
        let mut spec = self.spec.unwrap_or_default();
        spec.suspend = Some(yes);
        Self {
            spec: Some(spec),
            ..self
        }
    }

    fn template(self, template: corev1::PodTemplateSpec) -> Self {
        let mut spec = self.spec.unwrap_or_default();
        spec.template = template;
        Self {
            spec: Some(spec),
            ..self
        }
    }

    fn pod(self, pod_spec: corev1::PodSpec) -> Self {
        let mut spec = self.spec.unwrap_or_default();
        spec.template.spec = Some(pod_spec);
        Self {
            spec: Some(spec),
            ..self
        }
    }

    fn ttl_seconds_after_finished(self, seconds: i32) -> Self {
        let mut spec = self.spec.unwrap_or_default();
        spec.ttl_seconds_after_finished = Some(seconds);
        Self {
            spec: Some(spec),
            ..self
        }
    }
}
