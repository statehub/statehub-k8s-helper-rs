//
// Copyright (c) 2022 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use anyhow::Context;
use serde_json as json;
use tokio::time;

use crate::ResourceExt as _;

use super::*;

impl super::Kubectl {
    pub fn delete_params(&self) -> api::DeleteParams {
        api::DeleteParams {
            grace_period_seconds: Some(0),
            ..api::DeleteParams::default()
        }
    }

    pub fn list_params(&self) -> api::ListParams {
        api::ListParams::default()
    }

    pub fn post_params(&self) -> api::PostParams {
        api::PostParams::default()
    }

    pub fn post_with_manager(&self, manager: &str) -> api::PostParams {
        let mut pp = self.post_params();
        pp.field_manager = Some(manager.to_string());
        pp
    }

    pub fn patch_params(&self) -> api::PatchParams {
        api::PatchParams::default()
    }

    pub fn patch_with_manager(&self, manager: &str) -> api::PatchParams {
        api::PatchParams::apply(manager)
    }

    pub fn log_params(&self) -> api::LogParams {
        api::LogParams::default()
    }

    pub fn log_params_previous(&self) -> api::LogParams {
        api::LogParams {
            previous: true,
            ..api::LogParams::default()
        }
    }

    pub async fn patch<K>(&self, object: K, api: kube::Api<K>) -> kube::Result<K>
    where
        K: kube::Resource + ser::Serialize + de::DeserializeOwned + Clone + fmt::Debug,
        <K as kube::Resource>::DynamicType: Default,
    {
        let name = object.name();
        let patch = api::Patch::Apply(clear_readonly_meta(object));
        let pp = self.patch_with_manager(&self.manager);
        api.patch(&name, &pp, &patch).await
    }

    pub async fn force_patch<K>(&self, object: K, api: kube::Api<K>) -> kube::Result<K>
    where
        K: kube::Resource + ser::Serialize + de::DeserializeOwned + Clone + fmt::Debug,
        <K as kube::Resource>::DynamicType: Default,
    {
        let name = object.name();
        let patch = api::Patch::Apply(clear_readonly_meta(object));
        let pp = self.patch_with_manager(&self.manager).force();
        api.patch(&name, &pp, &patch).await
    }

    pub fn api<T>(&self) -> api::Api<T>
    where
        T: kube::Resource,
        <T as kube::Resource>::DynamicType: Default,
    {
        let client = self.client();
        api::Api::<T>::all(client)
    }

    pub fn default_namespaced_api<T>(&self) -> api::Api<T>
    where
        T: kube::Resource,
        <T as kube::Resource>::DynamicType: Default,
    {
        let client = self.client();
        api::Api::<T>::default_namespaced(client)
    }

    pub fn namespaced_api<T>(&self, namespace: &str) -> api::Api<T>
    where
        T: kube::Resource,
        <T as kube::Resource>::DynamicType: Default,
    {
        let client = self.client();
        api::Api::<T>::namespaced(client, namespace)
    }

    pub fn configmaps<'a>(
        &self,
        namespace: impl Into<Option<&'a str>>,
    ) -> api::Api<corev1::ConfigMap> {
        if let Some(namespace) = namespace.into() {
            self.namespaced_api::<corev1::ConfigMap>(namespace)
        } else {
            self.default_namespaced_api::<corev1::ConfigMap>()
        }
    }

    pub fn daemonsets<'a>(
        &self,
        namespace: impl Into<Option<&'a str>>,
    ) -> api::Api<appsv1::DaemonSet> {
        if let Some(namespace) = namespace.into() {
            self.namespaced_api::<appsv1::DaemonSet>(namespace)
        } else {
            self.default_namespaced_api::<appsv1::DaemonSet>()
        }
    }

    pub fn deployments<'a>(
        &self,
        namespace: impl Into<Option<&'a str>>,
    ) -> api::Api<appsv1::Deployment> {
        if let Some(namespace) = namespace.into() {
            self.namespaced_api::<appsv1::Deployment>(namespace)
        } else {
            self.default_namespaced_api::<appsv1::Deployment>()
        }
    }

    pub fn nodes(&self) -> api::Api<corev1::Node> {
        self.api::<corev1::Node>()
    }

    pub fn crds(&self) -> api::Api<apiextensionsv1::CustomResourceDefinition> {
        self.api::<apiextensionsv1::CustomResourceDefinition>()
    }

    pub fn namespaces(&self) -> api::Api<corev1::Namespace> {
        self.api::<corev1::Namespace>()
    }

    pub fn secrets<'a>(&self, namespace: impl Into<Option<&'a str>>) -> api::Api<corev1::Secret> {
        if let Some(namespace) = namespace.into() {
            self.namespaced_api::<corev1::Secret>(namespace)
        } else {
            self.default_namespaced_api::<corev1::Secret>()
        }
    }

    pub fn pods<'a>(&self, namespace: impl Into<Option<&'a str>>) -> api::Api<corev1::Pod> {
        if let Some(namespace) = namespace.into() {
            self.namespaced_api::<corev1::Pod>(namespace)
        } else {
            self.default_namespaced_api::<corev1::Pod>()
        }
    }

    pub async fn get_deployment<'a>(
        &self,
        name: &str,
        namespace: impl Into<Option<&'a str>>,
    ) -> kube::Result<appsv1::Deployment> {
        self.deployments(namespace).get(name).await
    }

    pub async fn get_namespace(&self, name: &str) -> kube::Result<corev1::Namespace> {
        self.namespaces().get(name).await
    }

    pub async fn get_secret(&self, name: &str) -> kube::Result<corev1::Secret> {
        self.secrets(None).get(name).await
    }

    pub async fn put_secret(
        &self,
        name: &str,
        r#type: &str,
        data: BTreeMap<String, ByteString>,
    ) -> kube::Result<corev1::Secret> {
        let secret: corev1::Secret = json::from_value(json::json!(
            {
                "metadata": {
                    "name": name
                },
                "data": data,
                "type": r#type
            }
        ))
        .expect("Failed to create corev1::Secret");

        self.ensure_default_namespaced_k_is_installed(secret).await
    }

    pub async fn get_config_map(&self, name: &str) -> kube::Result<corev1::ConfigMap> {
        self.configmaps(None).get(name).await
    }

    pub async fn put_config_map(
        &self,
        name: &str,
        data: BTreeMap<String, String>,
    ) -> kube::Result<corev1::ConfigMap> {
        const NOT_FOUND: http::StatusCode = http::StatusCode::NOT_FOUND;

        let pp = self.post_params();
        let configmaps = self.configmaps(None);
        let mut cm = corev1::ConfigMap::new(name).data(data);

        match configmaps.get(name).await {
            Ok(existing) => {
                if let Some(version) = existing.resource_version() {
                    cm = cm.with_resource_version(version);
                }
                configmaps.replace(name, &pp, &cm).await
            }
            Err(kube::Error::Api(e)) if e.code == NOT_FOUND.as_u16() => {
                configmaps.create(&pp, &cm).await
            }
            Err(e) => Err(e),
        }
    }

    const MAX_RETRIES: u8 = u8::MAX;
    pub async fn update<K>(
        client: kube::api::Api<K>,
        name: &str,
        update_fn: impl FnMut(&mut K),
    ) -> kube::Result<K>
    where
        K: std::fmt::Debug
            + Clone
            + serde::de::DeserializeOwned
            + serde::Serialize
            + kube::Resource,
    {
        Self::entry(
            client,
            name,
            Self::MAX_RETRIES,
            update_fn,
            None::<fn() -> K>,
        )
        .await
    }

    pub async fn update_with_default<K>(
        client: kube::api::Api<K>,
        name: &str,
        update_fn: impl FnMut(&mut K),
        default: impl Fn() -> K,
    ) -> kube::Result<K>
    where
        K: std::fmt::Debug
            + Clone
            + serde::de::DeserializeOwned
            + serde::Serialize
            + kube::Resource,
    {
        Self::entry(client, name, Self::MAX_RETRIES, update_fn, Some(default)).await
    }

    /// try to `commit` changes (`update_fn`)
    /// to object `K`
    /// of name: `name`
    /// for at most `max_retries` retries
    ///
    /// Note, errors of type `kube::Error::Api` are synthesized in cases of failures
    pub async fn entry<K>(
        client: kube::api::Api<K>,
        name: &str,
        max_retries: u8,
        mut update_fn: impl FnMut(&mut K),
        default: Option<impl Fn() -> K>,
    ) -> kube::Result<K>
    where
        K: std::fmt::Debug
            + Clone
            + serde::de::DeserializeOwned
            + serde::Serialize
            + kube::Resource,
    {
        /// 1000 minutes maximum backoff times
        const MAX_BACKOFF_MILLI: u64 = 1_000 * 1_000 * 60;
        const NOT_FOUND: http::StatusCode = http::StatusCode::NOT_FOUND;
        const CONFLICT: http::StatusCode = http::StatusCode::CONFLICT;
        const UNPROCESSABLE_ENTITY: http::StatusCode = http::StatusCode::UNPROCESSABLE_ENTITY;

        use kube::api::entry::CommitError;
        use kube::api::entry::Entry;
        use kube::error::ErrorResponse;
        let pp = api::PostParams::default();
        let default = default.as_ref();

        let mut last_err = None;
        for retry_count in 0..max_retries {
            let entry = match client.entry(name).await? {
                Entry::Occupied(entry) => entry,
                Entry::Vacant(entry) => {
                    let default = default.ok_or_else(|| {
                        kube::Error::Api(ErrorResponse {
                            status: NOT_FOUND.to_string(),
                            code: NOT_FOUND.as_u16(),
                            message: String::new(),
                            reason: String::new(),
                        })
                    })?;
                    entry.insert(default())
                }
            };
            let mut entry = entry.and_modify(&mut update_fn);

            let e = match entry.commit(&pp).await {
                Err(CommitError::Save(kube::Error::Api(e))) if e.code == CONFLICT.as_u16() => e,
                // return for all other case
                Ok(()) => return Ok(entry.into_object()),
                Err(CommitError::Save(e)) => return Err(e),
                Err(ref err @ CommitError::Validate(ref e)) => {
                    return Err(kube::Error::Api(ErrorResponse {
                        status: UNPROCESSABLE_ENTITY.to_string(),
                        code: UNPROCESSABLE_ENTITY.as_u16(),
                        message: e.to_string(),
                        reason: err.to_string(),
                    }))
                }
            };
            last_err = Some(e);

            let millis = 1u64
                .checked_shl(retry_count as u32)
                .and_then(|pow2| pow2.checked_mul(10))
                .filter(|millis| *millis < MAX_BACKOFF_MILLI)
                .unwrap_or(MAX_BACKOFF_MILLI);

            time::sleep(time::Duration::from_millis(millis)).await
        }

        let e = last_err.unwrap_or_else(|| kube::error::ErrorResponse {
            code: CONFLICT.as_u16(),
            status: CONFLICT.as_str().to_string(),
            message: String::new(),
            reason: String::new(),
        });
        Err(kube::Error::Api(e))
    }

    /// get all of the items `K` allow to filtered by `lp`
    pub async fn list_all<K>(
        client: kube::api::Api<K>,
        mut lp: api::ListParams,
    ) -> kube::Result<Vec<K>>
    where
        K: Clone + serde::de::DeserializeOwned + std::fmt::Debug,
    {
        let mut items = vec![];
        loop {
            let object_list = client.list(&lp).await?;
            lp.continue_token = object_list.metadata.continue_;
            items.extend(object_list.items);
            if object_list.metadata.remaining_item_count.unwrap_or(0) == 0 {
                break;
            }
        }
        Ok(items)
    }

    pub async fn update_config_map(
        &self,
        name: &str,
        mut update_fn: impl FnMut(&mut BTreeMap<String, String>),
    ) -> kube::Result<corev1::ConfigMap> {
        Self::update_with_default(
            self.configmaps(None),
            name,
            |cm| update_fn(cm.data_mut()),
            || corev1::ConfigMap::new(name),
        )
        .await
    }

    pub async fn allocate_in_configmap<K, V, F>(
        &self,
        name: &str,
        key: K,
        or_insert_with: F,
    ) -> anyhow::Result<V>
    where
        K: Ord + ToString + std::str::FromStr + Clone,
        K::Err: std::fmt::Debug + Send + Sync + 'static,
        V: ser::Serialize + de::DeserializeOwned + Clone,
        F: Fn(&BTreeMap<K, V>) -> anyhow::Result<V>,
    {
        let mut value = None;
        self.update_json_map(name, |cm_data: &mut BTreeMap<K, V>| {
            let v = if let Some(v) = cm_data.get(&key).cloned() {
                Ok(v)
            } else {
                let res = or_insert_with(cm_data);
                if let Ok(v) = &res {
                    cm_data.insert(key.clone(), v.clone());
                }
                res
            };
            value = Some(v);
        })
        .await?;
        value.context("value not allocated")?
    }

    /// allocate multiple `keys` from configmap of `name`, in one transactions
    /// useful to avoid contention over configmap `name`
    pub async fn allocate_multi_in_configmap<K, V, F>(
        &self,
        name: &str,
        keys: BTreeSet<K>,
        or_insert_with: F,
    ) -> anyhow::Result<BTreeMap<K, V>>
    where
        K: Ord + ToString + std::str::FromStr + Clone,
        K::Err: std::fmt::Debug + Send + Sync + 'static,
        V: ser::Serialize + de::DeserializeOwned + Clone,
        F: Fn(&BTreeMap<K, V>, usize) -> anyhow::Result<Vec<V>>,
    {
        let mut output = BTreeMap::new();

        let mut err = Ok(());
        self.update_json_map(name, |cm_data: &mut BTreeMap<K, V>| {
            let (not_found, mut found): (Vec<_>, BTreeMap<K, V>) = keys
                .iter()
                .cloned()
                .partition_map(|key| match cm_data.get(&key).cloned() {
                    Some(value) => itertools::Either::Right((key, value)),
                    None => itertools::Either::Left(key),
                });

            err = match or_insert_with(cm_data, not_found.len()) {
                Ok(values) if values.len() == not_found.len() => {
                    let not_found: Vec<_> = not_found.into_iter().zip(values).collect();
                    cm_data.extend(not_found.clone());
                    found.extend(not_found);
                    output = found;
                    Ok(())
                }
                Err(e) => Err(e),
                Ok(values) => Err(anyhow::anyhow!(
                    "requested {} keys, but allocated {}",
                    keys.len(),
                    values.len()
                )),
            };
        })
        .await?;
        err?;

        Ok(output)
    }

    pub async fn deallocate_in_configmap<K, V>(
        &self,
        name: &str,
        key: &K,
    ) -> anyhow::Result<Option<V>>
    where
        K: Ord + ToString + std::str::FromStr,
        K::Err: std::fmt::Debug + Send + Sync + 'static,
        V: ser::Serialize + de::DeserializeOwned,
    {
        let mut value = None;

        self.update_json_map(name, |cm_data: &mut BTreeMap<K, V>| {
            value = cm_data.remove(key);
        })
        .await?;

        Ok(value)
    }

    pub async fn deallocate_multi_in_configmap<K, V>(
        &self,
        name: &str,
        keys: &BTreeSet<K>,
    ) -> anyhow::Result<BTreeMap<K, V>>
    where
        K: Ord + ToString + std::str::FromStr,
        K::Err: std::fmt::Debug + Send + Sync + 'static,
        V: ser::Serialize + de::DeserializeOwned,
    {
        let mut values = BTreeMap::new();

        self.update_json_map(name, |cm_data: &mut BTreeMap<K, V>| {
            values.extend(keys.iter().filter_map(|key| cm_data.remove_entry(key)))
        })
        .await?;

        Ok(values)
    }

    pub async fn update_json_map<K, V>(
        &self,
        name: &str,
        mut update_fn: impl FnMut(&mut BTreeMap<K, V>),
    ) -> anyhow::Result<corev1::ConfigMap>
    where
        K: Ord + ToString + std::str::FromStr,
        K::Err: std::fmt::Debug + Send + Sync + 'static,
        V: ser::Serialize + de::DeserializeOwned,
    {
        // rv used to make sure that update has been applied successfully
        let mut rv = Ok(());
        let map = self
            .update_config_map(name, |data| {
                rv = try_update_json_configmap(data, &mut update_fn)
            })
            .await?;
        rv?;
        Ok(map)
    }

    pub async fn list_daemonsets<'a>(
        &self,
        namespace: impl Into<Option<&'a str>>,
    ) -> kube::Result<Vec<appsv1::DaemonSet>> {
        let lp = self.list_params();
        self.daemonsets(namespace)
            .list(&lp)
            .await
            .map(|object| object.items)
    }

    pub async fn list_deployments<'a>(
        &self,
        namespace: impl Into<Option<&'a str>>,
    ) -> kube::Result<Vec<appsv1::Deployment>> {
        let lp = self.list_params();
        self.deployments(namespace)
            .list(&lp)
            .await
            .map(|object| object.items)
    }

    pub async fn list_pods<'a>(
        &self,
        namespace: impl Into<Option<&'a str>>,
    ) -> kube::Result<Vec<corev1::Pod>> {
        let lp = self.list_params();
        self.pods(namespace)
            .list(&lp)
            .await
            .map(|object| object.items)
    }

    pub async fn list_crds(&self) -> kube::Result<Vec<apiextensionsv1::CustomResourceDefinition>> {
        let lp = self.list_params();
        self.crds().list(&lp).await.map(|object| object.items)
    }

    pub(super) async fn ensure_global_k_is_installed_impl<K>(
        &self,
        object: K,
        force: bool,
    ) -> kube::Result<K>
    where
        K: kube::ResourceExt + ser::Serialize + de::DeserializeOwned + Clone + fmt::Debug,
        <K as kube::Resource>::DynamicType: Default,
    {
        if force {
            self.force_patch(object, self.api()).await
        } else {
            self.patch(object, self.api()).await
        }
    }

    pub(crate) async fn ensure_namespaced_k_is_installed_impl<K>(
        &self,
        object: K,
        namespace: &str,
        force: bool,
    ) -> kube::Result<K>
    where
        K: kube::Resource + ser::Serialize + de::DeserializeOwned + Clone + fmt::Debug,
        <K as kube::Resource>::DynamicType: Default,
    {
        if force {
            self.force_patch(object, self.namespaced_api(namespace))
                .await
        } else {
            self.patch(object, self.namespaced_api(namespace)).await
        }
    }

    pub(crate) async fn ensure_default_namespaced_k_is_installed_impl<K>(
        &self,
        object: K,
        force: bool,
    ) -> kube::Result<K>
    where
        K: kube::Resource + ser::Serialize + de::DeserializeOwned + Clone + fmt::Debug,
        <K as kube::Resource>::DynamicType: Default,
    {
        if force {
            self.force_patch(object, self.default_namespaced_api())
                .await
        } else {
            self.patch(object, self.default_namespaced_api()).await
        }
    }
}

/// clear read-only fields in object-meta, `v1/object_meta.rs` `ObjectMeta`
/// ``
///     /// CreationTimestamp is a timestamp representing the server time when this object was created. It is not guaranteed to be set in happens-before order across separate operations. Clients may not set this value. It is represented in RFC3339 form and is in UTC.
///     ///
///     /// Populated by the system. Read-only. Null for lists. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#metadata
///     pub creation_timestamp: Option<crate::apimachinery::pkg::apis::meta::v1::Time>,
///     /// DeletionTimestamp is RFC 3339 date and time at which this resource will be deleted. This field is set by the server when a graceful deletion is requested by the user, and is not directly settable by a client. The resource is expected to be deleted (no longer visible from resource lists, and not reachable by name) after the time in this field, once the finalizers list is empty. As long as the finalizers list contains items, deletion is blocked. Once the deletionTimestamp is set, this value may not be unset or be set further into the future, although it may be shortened or the resource may be deleted prior to this time. For example, a user may request that a pod is deleted in 30 seconds. The Kubelet will react by sending a graceful termination signal to the containers in the pod. After that 30 seconds, the Kubelet will send a hard termination signal (SIGKILL) to the container and after cleanup, remove the pod from the API. In the presence of network partitions, this object may still exist after this timestamp, until an administrator or automated process can determine the resource is fully terminated. If not set, graceful deletion of the object has not been requested.
///     ///
///     /// Populated by the system when a graceful deletion is requested. Read-only. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#metadata
///     pub deletion_timestamp: Option<crate::apimachinery::pkg::apis::meta::v1::Time>,
///     /// An opaque value that represents the internal version of this object that can be used by clients to determine when objects have changed. May be used for optimistic concurrency, change detection, and the watch operation on a resource or set of resources. Clients must treat these values as opaque and passed unmodified back to the server. They may only be valid for a particular resource or set of resources.
///     ///
///     /// Populated by the system. Read-only. Value must be treated as opaque by clients and . More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#concurrency-control-and-consistency
///     pub resource_version: Option<String>,
///     /// SelfLink is a URL representing this object. Populated by the system. Read-only.
///     ///
///     /// DEPRECATED Kubernetes will stop propagating this field in 1.20 release and the field is planned to be removed in 1.21 release.
///     pub self_link: Option<String>,
///     /// UID is the unique in time and space value for this object. It is typically generated by the server on successful creation of a resource and is not allowed to change on PUT operations.
///     ///
///     /// Populated by the system. Read-only. More info: http://kubernetes.io/docs/user-guide/identifiers#uids
///     pub uid: Option<String>,
/// ``
fn clear_readonly_meta<K>(mut obj: K) -> K
where
    K: kube::Resource,
{
    let meta = obj.meta_mut();
    meta.creation_timestamp = None;
    meta.deletion_timestamp = None;
    meta.resource_version = None;
    meta.self_link = None;
    meta.uid = None;
    obj
}
/// convert `configmap` key: `K`
///
/// #errors:
///
/// from https://kubernetes.io/docs/concepts/configuration/configmap/
/// ```field must consist of alphanumeric characters, -, _ or ..```
fn try_key_to_string<K>(key: &K) -> anyhow::Result<String>
where
    K: ToString,
{
    let key = key.to_string();

    anyhow::ensure!(
        key.chars()
            .all(|ch| ch.is_alphanumeric() || ['-', '_', ','].contains(&ch)),
        "{} contains non alphanumeric or in ['-', '_', ','] character",
        key
    );
    Ok(key)
}
/// an try-update function that takes a `confimap.data`: `BTreeMap<String, String>`
/// and applies `update_fn: impl FnMut(&mut BTreeMap<K, V>)`
fn try_update_json_configmap<K, V>(
    cm_data: &mut BTreeMap<String, String>,
    mut update_fn: impl FnMut(&mut BTreeMap<K, V>),
) -> anyhow::Result<()>
where
    K: Ord + ToString + std::str::FromStr,
    K::Err: std::fmt::Debug + Send + Sync + 'static,
    V: ser::Serialize + de::DeserializeOwned,
{
    use anyhow::anyhow;
    use serde_json::{from_str, to_string};
    use std::any::type_name;

    let mut data: BTreeMap<K, V> = cm_data
        .iter()
        .map(|(key, value)| {
            let key = key.parse().map_err(|e: K::Err| anyhow!("{:?}", e))?;
            let value = from_str(value)?;
            Ok((key, value))
        })
        .collect::<anyhow::Result<_>>()
        .with_context(|| {
            format!(
                "deserialize configmap {:?} data into BTreeMap<{}, {}>",
                cm_data,
                type_name::<K>(),
                type_name::<V>(),
            )
        })?;
    update_fn(&mut data);

    *cm_data = data
        .iter()
        .map(|(key, value)| Ok((try_key_to_string(key)?, to_string(value)?)))
        .collect::<anyhow::Result<_>>()
        .with_context(|| format!("serialize configmap {:?} after update", cm_data))?;

    Ok(())
}
