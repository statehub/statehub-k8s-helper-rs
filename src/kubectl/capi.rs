//
// Copyright (c) 2022 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use anyhow::Context;
use kube::config;

use super::*;

impl Kubectl {
    pub async fn load_config_from_secret(&self, cluster: &str) -> kube::Result<kube::Config> {
        let text = self.capi_kubeconfig(cluster).await?;
        load_config_from_text(&text)
            .await
            .map_err(from_kubeconfig_error)
    }

    pub async fn capi_kubeconfig(&self, cluster: &str) -> kube::Result<String> {
        let secret = format!("{}-kubeconfig", cluster);
        self.load_text_from_secret(&secret, "value").await
    }

    pub async fn capi_clusters(&self) -> kube::Result<Vec<capiv1::Cluster>> {
        let lp = self.list_params();
        self.default_namespaced_api()
            .list(&lp)
            .await
            .map(|object| object.items)
    }

    pub async fn capi_cluster(&self, cluster: &str) -> kube::Result<capiv1::Cluster> {
        self.default_namespaced_api().get(cluster).await
    }

    pub async fn capi_infrastructure_aws(&self, name: &str) -> kube::Result<capiv1::AWSCluster> {
        self.default_namespaced_api().get(name).await
    }

    pub async fn capi_control_plane_kubeadm(
        &self,
        name: &str,
    ) -> kube::Result<capiv1::KubeadmControlPlane> {
        self.default_namespaced_api().get(name).await
    }

    pub async fn capi_infrastructure(
        &self,
        cluster: &capiv1::Cluster,
    ) -> anyhow::Result<capiv1::Infrastructure> {
        let infrastructure = cluster.infrastructure();
        let infrastructure = typed_ref(infrastructure).context("Invalid infrastructure_ref")?;

        let infrastructure = match infrastructure.kind.as_str() {
            "AWSCluster" => self
                .capi_infrastructure_aws(&infrastructure.name)
                .await
                .map(capiv1::Infrastructure::Aws)?,

            other => anyhow::bail!("Unknown infrastructure kind: '{}'", other),
        };
        Ok(infrastructure)
    }

    pub async fn capi_control_plane(
        &self,
        cluster: &capiv1::Cluster,
    ) -> anyhow::Result<capiv1::ControlPlane> {
        let control_plane = cluster.control_plane();
        let control_plane = typed_ref(control_plane).context("Invalid control_plane_ref")?;

        let control_plane = match control_plane.kind.as_str() {
            "KubeadmControlPlane" => self
                .capi_control_plane_kubeadm(&control_plane.name)
                .await
                .map(capiv1::ControlPlane::Kubeadm)?,

            other => anyhow::bail!("Unknown control plane kind: '{}'", other),
        };
        Ok(control_plane)
    }
}

async fn load_config_from_text(text: &str) -> Result<config::Config, config::KubeconfigError> {
    let kubeconfig = config::Kubeconfig::from_yaml(text)?;
    let options = config::KubeConfigOptions::default();
    config::Config::from_custom_kubeconfig(kubeconfig, &options).await
}
