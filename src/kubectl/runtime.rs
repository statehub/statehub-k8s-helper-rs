//
// Copyright (c) 2022 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use super::*;

impl Kubectl {
    pub async fn create_global_crd(
        &self,
        crd: apiextensionsv1::CustomResourceDefinition,
    ) -> anyhow::Result<apiextensionsv1::CustomResourceDefinition> {
        use anyhow::Context;
        use kube::runtime::wait;
        use wait::conditions::is_crd_established;

        let crd = self.create_global_crd_impl(crd).await?;
        let name = crd
            .metadata
            .name
            .as_ref()
            .context("crd does not have name")?;

        wait::await_condition(self.crds(), name, is_crd_established())
            .await
            .context("failed to established crd")?;
        Ok(crd)
    }

    pub async fn ensure_crd_is_installed(
        &self,
        crd: apiextensionsv1::CustomResourceDefinition,
    ) -> anyhow::Result<()> {
        self.create_global_crd(crd).await?;
        Ok(())
    }

    async fn create_global_crd_impl(
        &self,
        crd: apiextensionsv1::CustomResourceDefinition,
    ) -> kube::Result<apiextensionsv1::CustomResourceDefinition> {
        use kube::Error::Api;
        let name = crd.name();
        let crds = self.crds();
        let conflict = http::StatusCode::CONFLICT.as_u16();

        let pp = self.post_params();
        let result = crds.create(&pp, &crd).await;
        match result {
            Err(Api(e)) if e.code == conflict && e.reason == "AlreadyExists" => {
                let existing = crds.get(&name).await?;
                // https://kubernetes.io/docs/tasks/extend-kubernetes/custom-resources/custom-resource-definitions/
                // One and only one version must be marked as the storage version.
                let crd_v = crd.spec.versions.iter().find(|v| v.storage);
                let existing_v = existing.spec.versions.iter().find(|v| v.storage);

                // make sure that the schemas match - if not propagate conflict error
                crd_v
                    .and_then(|v| v.schema.as_ref())
                    .zip(existing_v.and_then(|v| v.schema.as_ref()))
                    .filter(|(crd_schema, existing_schema)| crd_schema == existing_schema)
                    .ok_or(Api(e))?;

                Ok(existing)
            }
            _ => result,
        }
    }
}
